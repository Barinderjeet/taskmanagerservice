﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBench;
using NBench.Util;
using TaskManagerBuisnessLib;
using TaskManagerEntitiesLib;

namespace PerformanceEval

{
    public class PerformanceEval
    {
        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
            NumberOfIterations = 1, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchMarkAddUpdate()
        {
            Task task = new Task();
            task.TaskName = "Test1";
            task.ParentTask = 2018;
            task.StartDate = System.DateTime.Now;
            task.EndDate = System.DateTime.Now; ;
            task.Flag = 0;
            task.Priority = 10;
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.Add(task);
            obj.Update(task);
            obj.UpdateEndDate(task);

        }


        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
           NumberOfIterations = 1, RunMode = RunMode.Throughput,
           RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]

        public void BenchMarkView()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.GetAll();
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be raapidly executed ",
         NumberOfIterations = 1, RunMode = RunMode.Throughput,
         RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]

        public void BenchMarkGetByTaskID()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.GetById(2012);
        }

        [PerfCleanup]
        public void CleanUp()
        {

        }
    }
}
