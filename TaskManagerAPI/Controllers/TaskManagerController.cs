﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagerBuisnessLib;
using TaskManagerEntitiesLib;

namespace TaskManagerAPI.Controllers
{
    public class TaskManagerController : ApiController
    {
        [Route("PostTask")]
        [HttpPost]
        public IHttpActionResult PostTask(Task task)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.Add(task);
            return Ok("Record Added");

        }

        [Route("DeleteTask")]
        [HttpDelete]
        public List<Task> DeleteTask(int id)
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            return obj.Delete(id);
        }

        [Route("UpdateTask")]
        [HttpPut]
        public IHttpActionResult PutTask(Task task)
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.Update(task);
            return Ok("Record Updated");
        }

        [Route("EndTask")]
        [HttpPut]
        public List<Task> UpdateEngDate(Task item)
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            return obj.UpdateEndDate(item);
        }

        [Route("GetAll")]
        [HttpGet]
        public List<Task> GetAll()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            return obj.GetAll();
        }

        [Route("GetByTaskName")]
        [HttpGet]
        public Task GetByTaskName(string TaskName)
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            return obj.GetByTaskName(TaskName);
        }

        [Route("GetById")]
        [HttpGet]
        public Task GetById(int TaskId)
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            return obj.GetById(TaskId);
        }



    }
}
