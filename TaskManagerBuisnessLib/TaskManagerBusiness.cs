﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagerDataLib;
using TaskManagerEntitiesLib;

namespace TaskManagerBuisnessLib
{
    public class TaskManagerBusiness
    {
        public void Add(Task item)
        {
            using (DataContext dbcontext = new DataContext())
            {
                dbcontext.Tasks.Add(item);
                dbcontext.SaveChanges();
            }
        }

        public List<Task> Delete(int id)
        {
            using (var ctx = new DataContext())
            {
                var task = ctx.Tasks.Where(s => s.TaskID == id)
                    .FirstOrDefault();
                ctx.Entry(task).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Tasks.ToList();
            }
        }
        public void test()
        {

        }
        public List<Task> UpdateEndDate(Task item)
        {
            using (DataContext dbcontext = new DataContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskID == item.TaskID);
                context.EndDate = System.DateTime.Now;
                context.Flag = 1;
                dbcontext.SaveChanges();
                return dbcontext.Tasks.ToList();
            }
        }

        public void Update(Task item)
        {
            using(DataContext dbContext = new DataContext())
                {
                var context = dbContext.Tasks.SingleOrDefault(x => x.TaskID == item.TaskID);
                context.TaskID = item.TaskID;
                context.TaskName = item.TaskName;
                context.ParentTask = item.ParentTask;
                context.Priority = item.Priority;
                context.StartDate = item.StartDate;
                context.EndDate = item.EndDate;
                dbContext.SaveChanges();                   
                }
        }

        public List<Task> GetAll()
        {
            using (DataContext dbcontext = new DataContext())
            {
                return dbcontext.Tasks.ToList();
            }
        }

        public Task GetByTaskName(string TaskName)
        {
            using (DataContext dbcontext = new DataContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskName == TaskName);
                return context;
            }
        }

        public Task GetById(int TaskID)
        {
            using (DataContext dbcontext = new DataContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskID == TaskID);
                return context;
            }
        }
     }
}
