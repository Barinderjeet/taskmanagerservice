﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

using TaskManagerEntitiesLib;

namespace TaskManagerDataLib
{
    public class DataContext:DbContext
    {
        public DataContext() : base("name= dbConnString")
        {
        }
        public DbSet<Task> Tasks { get; set; }
    }
}
