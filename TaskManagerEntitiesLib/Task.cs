﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace TaskManagerEntitiesLib
{
    public class Task
    {
        [Key]
        public int TaskID { get; set; }
        public int ParentTask { get; set; }
        public string TaskName { get; set; }
        public int Priority { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Flag { get; set; }


    }
}
