﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TaskManagerEntitiesLib;
using TaskManagerBuisnessLib;


namespace TestingBusinessLayer
{
    [TestFixture]
    public class TestBusinessLayer
    {

        [Test]
        public void TestGetAll()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            int count = obj.GetAll().Count;
            Assert.Greater(count, 0);
        }

        [Test]
        public void TestGetTaskId()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task itemId = obj.GetById(9);
            Assert.AreEqual(9, itemId.TaskID);
        }

        [Test]
        public void TestTaskAdd()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task item = new Task();
            item.TaskName = "TaskName1";
            item.ParentTask = 15;
            item.Priority = 9;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.Add(item);
            Task testItem = obj.GetByTaskName("TaskName1");
            Assert.AreEqual("TaskName1", testItem.TaskName);
        }

        [Test]
        public void TestDelete()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.Delete(4);
            Task Task = obj.GetById(4);
            Assert.AreEqual(null, Task);
        }

        [Test]
        public void TestTaskUpdate()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task item = new Task();
            item.TaskID = 12;
            item.TaskName = "TaskName1";
            item.Priority = 15;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.Update(item);
            Task updatedItems = obj.GetByTaskName("TaskName1");
            Assert.AreEqual(15, updatedItems.Priority);
                
        }

        [Test]
        public void TestEndaTaskUpdate()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task item = new Task();
            item.TaskID = 12;
            item.TaskName = "TaskName1";
            item.Priority = 9;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.UpdateEndDate(item);
            Task itemAfterUpdate = obj.GetByTaskName("TaskName1");
            Assert.AreEqual(1, itemAfterUpdate.Flag);
        }
    }
}
