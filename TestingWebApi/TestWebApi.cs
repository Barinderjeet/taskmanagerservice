﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Web.Http;
using TaskManagerAPI.Controllers;
using TaskManagerEntitiesLib;

namespace TestingWebApi
{
    [TestFixture]
    public class TestWebApi
    {
        [Test]
        public void TestGetAllWebApi()
        {
            TaskManagerController obj = new TaskManagerController();
            List<Task> taskDetails = obj.GetAll();
            Assert.Greater(taskDetails.Count(), 0);
        }

        [Test]
        public void TestGetByTaskIdWebApi()
        {
            TaskManagerController obj = new TaskManagerController();
            Task item = obj.GetById(10);
            Assert.AreEqual(10, item.TaskID);
        }

        [Test]
        public void TestAddTaskWebApi()
        {
            TaskManagerController obj = new TaskManagerController();
            Task item = new Task();
            item.TaskName = "TaskName5";
            item.ParentTask = 16;
            item.Priority = 9;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            IHttpActionResult ok = obj.PostTask(item);
            Task testItem = obj.GetByTaskName("TaskName5");
            Assert.AreEqual("TaskName5", testItem.TaskName);
        }

        [Test]
        public void TestDeletetaskWebApi()
        {
            TaskManagerController obj = new TaskManagerController();
            List<Task> task = obj.DeleteTask(10);
            Assert.AreEqual(1, task.Count());
        }

        [Test]
        public void TestUpdateTaskWebApi()
        {
            TaskManagerController obj = new TaskManagerController();
            Task item = new Task();
            item.TaskID = 3012;
            item.TaskName = "TaskName";
            item.Priority = 11;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.PutTask(item);
            Task ItemUpdate = obj.GetByTaskName("TaskName");
            Assert.AreEqual(11, ItemUpdate.Priority);
        }

        [Test]
        public void TestEndTaskWebApi()
        {
            TaskManagerController obj = new TaskManagerController();
            Task item = new Task();
            item.TaskID = 8;
            item.TaskName = "test3";
            item.Priority = 2;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.UpdateEngDate(item);
            Task ItemUpdate = obj.GetByTaskName("test3");
            Assert.AreEqual(1, ItemUpdate.Flag);
        }
    }
}
